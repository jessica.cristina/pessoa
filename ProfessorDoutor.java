package model;

public class ProfessorDoutor extends Professor {

	private int anoDeObtencaoDoutorado;
	private String instituicaoDoDoutorado;
	
	void imprimeDados()
	{
		super.Professor();
		System.out.println("Doutorado obtido em" + this.instituicaoDoDoutorado + "em" + this.anoDeObtencaoDoutorado);
	}

	public int getAnoDeObtencaoDoutorado() {
		return anoDeObtencaoDoutorado;
	}

	public void setAnoDeObtencaoDoutorado(int anoDeObtencaoDoutorado) {
		this.anoDeObtencaoDoutorado = anoDeObtencaoDoutorado;
	}

	public String getInstituicaoDoDoutorado() {
		return instituicaoDoDoutorado;
	}

	public void setInstituicaoDoDoutorado(String instituicaoDoDoutorado) {
		this.instituicaoDoDoutorado = instituicaoDoDoutorado;
	}
		
	

		
}
