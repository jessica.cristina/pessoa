package model;

public class Aluno extends Pessoa {

	private String curso;
	private int anoDeIngresso;
	
	void Pessoa()
	{
		super.Pessoa();
		System.out.println("Ingresso no curso" + this.curso + "em" + this.anoDeIngresso);
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public int getAnoDeIngresso() {
		return anoDeIngresso;
	}

	public void setAnoDeIngresso(int anoDeIngresso) {
		this.anoDeIngresso = anoDeIngresso;
	}
	
}
