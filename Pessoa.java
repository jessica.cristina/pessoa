package model;

public class Pessoa {

	
	private String nome;
	private char sexo;
	private String CPF;
	private String RG;
	private int anoDeNacimento;
	

	 void Pessoa() {
		 
		if (sexo == 'F' )
			System .out.println("A Sra. " + this.nome + " nasceu no ano " + this.anoDeNacimento + " CPF " + this.CPF + "  RG " + this.RG );
		else 
			System .out.println("A Sr. " + this.nome + " nasceu no ano " + this.anoDeNacimento + " CPF " + this.CPF + "  RG " + this.RG );
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public char getSexo() {
		return sexo;
	}


	public void setSexo(char sexo) {
		this.sexo = sexo;
	}


	public String getCPF() {
		return CPF;
	}


	public void setCPF(String cPF) {
		CPF = cPF;
	}


	public String getRG() {
		return RG;
	}


	public void setRG(String rG) {
		RG = rG;
	}


	public int getAnoDeNacimento() {
		return anoDeNacimento;
	}


	public void setAnoDeNacimento(int anoDeNacimento) {
		this.anoDeNacimento = anoDeNacimento;
	}
	
	
}
